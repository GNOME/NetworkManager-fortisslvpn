# Polish translation for NetworkManager-fortisslvpn.
# Copyright © 2008-2022 the NetworkManager-fortisslvpn authors.
# This file is distributed under the same license as the NetworkManager-fortisslvpn package.
# Tomasz Dominikowski <dominikowski@gmail.com>, 2008-2009.
# Piotr Drąg <piotrdrag@gmail.com>, 2010-2022.
# Aviary.pl <community-poland@mozilla.org>, 2008-2022.
#
msgid ""
msgstr ""
"Project-Id-Version: NetworkManager-fortisslvpn\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/NetworkManager-"
"fortisslvpn/issues\n"
"POT-Creation-Date: 2022-03-13 12:18+0000\n"
"PO-Revision-Date: 2022-03-13 13:14+0100\n"
"Last-Translator: Piotr Drąg <piotrdrag@gmail.com>\n"
"Language-Team: Polish <community-poland@mozilla.org>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2;\n"

#: appdata/network-manager-fortisslvpn.metainfo.xml.in:9
msgid "Fortinet SSLVPN client"
msgstr "Klient SSLVPN Fortinet"

#: appdata/network-manager-fortisslvpn.metainfo.xml.in:10
msgid "Client for Fortinet SSLVPN virtual private networks"
msgstr "Klient wirtualnych sieci prywatnych SSLVPN Fortinet"

#: appdata/network-manager-fortisslvpn.metainfo.xml.in:24
msgid ""
"Support for configuring Fortinet SSLVPN virtual private network connections."
msgstr ""
"Obsługa konfigurowania połączeń wirtualnych sieci prywatnych SSLVPN Fortinet."

#: appdata/network-manager-fortisslvpn.metainfo.xml.in:32
msgid "The advanced options dialog"
msgstr "Okno zaawansowanych opcji"

#: appdata/network-manager-fortisslvpn.metainfo.xml.in:41
msgid "The NetworkManager Developers"
msgstr "Programiści projektu NetworkManager"

#. Otherwise, we have no saved password, or the password flags indicated
#. * that the password should never be saved.
#.
#: auth-dialog/main.c:165
#, c-format
msgid "You need to authenticate to access the Virtual Private Network “%s”."
msgstr ""
"Aby uzyskać dostęp do wirtualnej sieci prywatnej „%s”, należy się "
"uwierzytelnić."

#: auth-dialog/main.c:174 auth-dialog/main.c:198
msgid "Authenticate VPN"
msgstr "Uwierzytelnianie VPN"

#: auth-dialog/main.c:177
msgid "Password"
msgstr "Hasło"

#: auth-dialog/main.c:179
msgid "Token"
msgstr "Token"

#: auth-dialog/main.c:202
msgid "_Token:"
msgstr "_Token:"

#: properties/nm-fortisslvpn-editor-plugin.c:38
msgid "Fortinet SSLVPN"
msgstr "SSLVPN Fortinet"

#: properties/nm-fortisslvpn-editor-plugin.c:39
msgid "Compatible with Fortinet SSLVPN servers."
msgstr "Zgodny z serwerami VPN Fortinet."

#: shared/nm-fortissl-properties.c:125
#, c-format
msgid "invalid gateway “%s”"
msgstr "nieprawidłowa brama „%s”"

#: shared/nm-fortissl-properties.c:133
#, c-format
msgid "invalid certificate authority “%s”"
msgstr "nieprawidłowe CA „%s”"

#: shared/nm-fortissl-properties.c:147
#, c-format
msgid "invalid integer property “%s”"
msgstr "nieprawidłowa własność liczby całkowitej „%s”"

#: shared/nm-fortissl-properties.c:157
#, c-format
msgid "invalid boolean property “%s” (not yes or no)"
msgstr ""
"nieprawidłowa własność zmiennej logicznej „%s” (nie wynosi „yes” lub „no”)"

#: shared/nm-fortissl-properties.c:164
#, c-format
msgid "unhandled property “%s” type %s"
msgstr "nieobsługiwana własność „%s” typu „%s”"

#: shared/nm-fortissl-properties.c:175
#, c-format
msgid "property “%s” invalid or not supported"
msgstr "własność „%s” jest nieprawidłowa lub nieobsługiwana"

#: shared/nm-fortissl-properties.c:192
msgid "No VPN configuration options."
msgstr "Brak opcji konfiguracji VPN."

#: shared/nm-fortissl-properties.c:212
#, c-format
msgid "Missing required option “%s”."
msgstr "Brak wymaganej opcji „%s”."

#: shared/nm-fortissl-properties.c:236
msgid "No VPN secrets!"
msgstr "Brak haseł VPN."

#: shared/nm-utils/nm-shared-utils.c:264
#, c-format
msgid "object class '%s' has no property named '%s'"
msgstr "klasa obiektów „%s” nie ma właściwości o nazwie „%s”"

#: shared/nm-utils/nm-shared-utils.c:271
#, c-format
msgid "property '%s' of object class '%s' is not writable"
msgstr "właściwość „%s” klasy obiektów „%s” nie jest zapisywalna"

#: shared/nm-utils/nm-shared-utils.c:278
#, c-format
msgid ""
"construct property \"%s\" for object '%s' can't be set after construction"
msgstr ""
"właściwość konstruktu „%s” dla obiektu „%s” nie może zostać ustawiona po "
"konstrukcji"

#: shared/nm-utils/nm-shared-utils.c:286
#, c-format
msgid "'%s::%s' is not a valid property name; '%s' is not a GObject subtype"
msgstr ""
"„%s::%s” nie jest prawidłową nazwą właściwości; „%s” nie jest podtypem "
"GObject"

#: shared/nm-utils/nm-shared-utils.c:295
#, c-format
msgid "unable to set property '%s' of type '%s' from value of type '%s'"
msgstr "nie można ustawić właściwości „%s” typu „%s” z wartości typu „%s”"

#: shared/nm-utils/nm-shared-utils.c:306
#, c-format
msgid ""
"value \"%s\" of type '%s' is invalid or out of range for property '%s' of "
"type '%s'"
msgstr ""
"wartość „%s” typu „%s” jest nieprawidłowa lub spoza zakresu dla właściwości "
"„%s” typu „%s”"

#: shared/nm-utils/nm-vpn-plugin-utils.c:69
#, c-format
msgid "unable to get editor plugin name: %s"
msgstr "nie można uzyskać nazwy wtyczki edytora: %s"

#: shared/nm-utils/nm-vpn-plugin-utils.c:103
#, c-format
msgid "missing plugin file \"%s\""
msgstr "brak pliku wtyczki „%s”"

#: shared/nm-utils/nm-vpn-plugin-utils.c:109
#, c-format
msgid "cannot load editor plugin: %s"
msgstr "nie można wczytać wtyczki edytora: %s"

#: shared/nm-utils/nm-vpn-plugin-utils.c:118
#, c-format
msgid "cannot load factory %s from plugin: %s"
msgstr "nie można wczytać generatora %s z wtyczki: %s"

#: shared/nm-utils/nm-vpn-plugin-utils.c:144
msgid "unknown error creating editor instance"
msgstr "nieznany błąd podczas tworzenia wystąpienia edytora"

#: src/nm-fortisslvpn-service.c:218
msgid "Could not find the openfortivpn binary."
msgstr "Nie można odnaleźć pliku binarnego openfortivpn."

#: src/nm-fortisslvpn-service.c:402
msgid "Missing VPN username."
msgstr "Brak nazwy użytkownika VPN."

#: src/nm-fortisslvpn-service.c:411
msgid "Missing or invalid VPN password."
msgstr "Brak hasła VPN lub jest nieprawidłowe."

#: src/nm-fortisslvpn-service.c:536
msgid "Got new secrets, but nobody asked for them."
msgstr "Otrzymano nowe hasła, ale nikt o nie nie prosił."

#: src/nm-fortisslvpn-service.c:720
msgid "Don’t quit when VPN connection terminates"
msgstr "Nie kończy działania, kiedy połączenie VPN jest kończone"

#: src/nm-fortisslvpn-service.c:721
msgid "Enable verbose debug logging (may expose passwords)"
msgstr "Włącza więcej komunikatów debugowania (może wyjawić hasła)"

#: src/nm-fortisslvpn-service.c:722
msgid "D-Bus name to use for this instance"
msgstr "Nazwa D-Bus dla tego wystąpienia"

#: src/nm-fortisslvpn-service.c:743
msgid ""
"nm-fortisslvpn-service provides integrated SSLVPN capability (compatible "
"with Fortinet) to NetworkManager."
msgstr ""
"nm-fortisslvpn-service dostarcza zintegrowaną możliwość SSLVPN (zgodną "
"z implementacją Fortinet) dla usługi NetworkManager."

#: properties/nm-fortisslvpn-dialog.ui:7
msgid "SSLVPN Advanced Options"
msgstr "Zaawansowane opcje SSLVPN"

#: properties/nm-fortisslvpn-dialog.ui:50
#: properties/nm-fortisslvpn-dialog.ui:351
msgid "Authentication"
msgstr "Uwierzytelnianie"

#: properties/nm-fortisslvpn-dialog.ui:67
msgid "_Realm"
msgstr "_Obszar"

#: properties/nm-fortisslvpn-dialog.ui:81
#: properties/nm-fortisslvpn-dialog.ui:337
msgid ""
"SSLVPN server IP or name.\n"
"config: the first parameter of fortisslvpn"
msgstr ""
"Adres IP lub nazwa serwera SSLVPN.\n"
"konfiguracja: pierwszy parametr fortisslvpn"

#: properties/nm-fortisslvpn-dialog.ui:95
msgid "Security"
msgstr "Zabezpieczenia"

#: properties/nm-fortisslvpn-dialog.ui:113
msgid "Trusted _certificate"
msgstr "Zaufany _certyfikat"

#: properties/nm-fortisslvpn-dialog.ui:127
msgid ""
"A SHA256 sum of the X509 certificate that will be accepted even if the "
"certificate is not trusted by a certificate authority."
msgstr ""
"Suma kontrolna SHA256 certyfikatu X.509 przyjmowanego, nawet jeśli "
"certyfikat nie jest zaufany przez CA."

#: properties/nm-fortisslvpn-dialog.ui:142
msgid "_One time password"
msgstr "_Hasło jednorazowe"

#: properties/nm-fortisslvpn-dialog.ui:185
msgid "Advanced Properties"
msgstr "Zaawansowane właściwości"

#: properties/nm-fortisslvpn-dialog.ui:193
msgid "_Cancel"
msgstr "_Anuluj"

#: properties/nm-fortisslvpn-dialog.ui:201
msgid "_Apply"
msgstr "_Zastosuj"

#: properties/nm-fortisslvpn-dialog.ui:253
msgid "Show password"
msgstr "Wyświetlanie hasła"

#: properties/nm-fortisslvpn-dialog.ui:269
msgid "Password passed to SSLVPN when prompted for it."
msgstr "Hasło przekazywane do SSLVPN."

#: properties/nm-fortisslvpn-dialog.ui:282
msgid ""
"Set the name used for authenticating the local system to the peer to "
"<name>.\n"
"config: user <name>"
msgstr ""
"Ustawia nazwę używaną do uwierzytelniania lokalnego systemu z partnerem na "
"<nazwę>.\n"
"konfiguracja: user <nazwa>"

#: properties/nm-fortisslvpn-dialog.ui:295
msgid "_Password"
msgstr "_Hasło"

#: properties/nm-fortisslvpn-dialog.ui:309
msgid "User _name"
msgstr "_Nazwa użytkownika"

#: properties/nm-fortisslvpn-dialog.ui:323
msgid "_Gateway"
msgstr "_Brama"

#: properties/nm-fortisslvpn-dialog.ui:369
msgid "General"
msgstr "Ogólne"

#: properties/nm-fortisslvpn-dialog.ui:413
msgid "A_dvanced…"
msgstr "Zaa_wansowane…"

#: properties/nm-fortisslvpn-dialog.ui:449
msgid "Default"
msgstr "Domyślne"
